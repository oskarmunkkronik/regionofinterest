classdef roiObject
properties
   mz(:,1) {mustBePositive}                        = [];
   intensity(:,1) {mustBePositive}                 = [];
   scanIndex(:,1) {mustBeInteger,mustBeNonnegative}   = [];
   mzIndex(:,1) {mustBeInteger,mustBeNonnegative}     = [];           
   regionIndex(:,1) {mustBeInteger,mustBeNonnegative} = [];
   regionLength(:,1) {mustBeInteger,mustBePositive}   = [];
   rt(:,1) {mustBePositive} = []; % Make must be monotonically increasing

end
methods
    function obj = roiObject(PeaksMat,rt)
        if (~nargin || isempty(PeaksMat)), return; end
        obj.intensity   = single(PeaksMat(:,2));
        obj.mzIndex     = uint32(cat(1,0,find(diff(PeaksMat(:,1)) ~= 0)));
        obj.mz          = single(PeaksMat(obj.mzIndex + 1,1));
        obj.scanIndex   = uint16(PeaksMat(:,3));
        obj.regionIndex = uint32(cat(1,0,find(diff(PeaksMat,4) > 0)));
        if (nargin < 2 || isempty(rt)), obj.rt = 1:max(obj.scanIndex); else, obj.rt = rt; end

    end
end

end