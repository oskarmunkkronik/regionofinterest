%MinRoi Check
function [PeaksMat,pos]=minroiFilter2(PeaksMat,minroi)
% %Filters out MS measurements,which is not doesn't have
PeaksMat=cat(1,PeaksMat,nan(1,size(PeaksMat,2)));
pos=zeros(size(PeaksMat,1),1);
Scan=1;
% Limit=size(PeaksMat,1)+1;
while Scan<size(PeaksMat,1)
    %Checks whether the number of consecutive peaks is >= than minroi:
    
    s=Scan;
%     e=Scan;
    nConsec=1;
    if PeaksMat(Scan,3)==PeaksMat(Scan+1,3)-1 && PeaksMat(Scan,4)==PeaksMat(Scan+1,4)
        while PeaksMat(Scan,3)==PeaksMat(Scan+1,3)-1 && PeaksMat(Scan,4)==PeaksMat(Scan+1,4) && Scan <size(PeaksMat,1)-1
            Scan=Scan+1;
            
%             e=e+1;
            nConsec=nConsec+1;
        end
        
    end
    
    %Checks whether the length of the number of consecutive points are >=
    %minimum number of consecutive points specified
    if  nConsec>=minroi%length(s:e)>=minroi%
        pos(s:Scan,1)=1;
    end
    Scan=Scan+1;
    
end

%Removes measurements with < number of consevutive points than specified by
%minroi:
% PeaksMat(pos==0,:)=[];
end
